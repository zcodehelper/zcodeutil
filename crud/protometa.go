package crud

import (
	"gitlab.com/zcodehelper/zcodeutil/proto"
)

const SQL_LEFT_PARENTHESES = " ( "
const SQL_RIGHT_PARENTHESES = " ) "
const SQL_INSERT_INTO = " INSERT INTO "
const SQL_INSERT_VALUES = " VALUES "
const SQL_UPDATE = " UPDATE "
const SQL_SET = " SET "
const SQL_EQUEAL = " = "
const SQL_COMMA = " , "
const SQL_SEMICOLON = " ; "
const SQL_SPACE = " "
const SQL_DELETE = " DELETE "
const SQL_FROM = " FROM "
const SQL_SELECT = " SELECT "
const SQL_WHERE = " WHERE "
const SQL_AND = " AND "
const SQL_OR = " OR "
const SQL_ANY = " ANY "
const SQL_1E1 = " 1 = 1 "
const SQL_ORDER_BY = " ORDER BY "
const SQL_INTERVAL = " INTERVAL "
const SQL_MINUTE = " MINUTE "
const SQL_MINUTES = " MINUTES "
const SQL_AS = " AS "
const SQL_APOSTROPHE = "'"
const SQL_PLUS = "+"
const SQL_MINUS = "-"
const sql_DATETIME = "datetime"
const SQL_TIMESTAMPADD = "TIMESTAMPADD"
const SQL_LIMIT = " LIMIT "
const SQL_LIMIT_1 = SQL_LIMIT + " 1 "
const SQL_OFFSET = " OFFSET "

type TprotoField struct {
	//field number
	No int32
	//field name
	Name string
	//field sql property
	//bit 0: primary key @dbprimary
	//bit 1: not null
	//bit 2: no update @dbnoupdate
	//bit 3: sql not @dbnot
	//bit 4: sql unique
	//bit 5: sql foreign key reference
	//bit 6: empty as null
	//bit 8-12(5bit):descriptorpb.FieldDescriptorProto_Type
	//bit 13-14(2bit):descriptorpb.FieldDescriptorProto_Label
	FieldOption uint32
}
type TprotoMsg struct {
	Name   string
	Fields []TprotoField
}

func (this *TprotoField) SQLPrimary() bool {
	return this.FieldOption&1 > 0
}
func (this *TprotoField) SQLNotNull() bool {
	return this.FieldOption&2 > 0
}
func (this *TprotoField) SQLNoUpdate() bool {
	return this.FieldOption&4 > 0
}
func (this *TprotoField) SQLNot() bool {
	return this.FieldOption&8 > 0
}
func (this *TprotoField) SQLUnique() bool {
	return this.FieldOption&16 > 0
}
func (this *TprotoField) ForeighRef() bool {
	return this.FieldOption&32 > 0
}
func (this *TprotoField) EmptyAsNull() bool {
	return this.FieldOption&64 > 0
}

func (this *TprotoField) IsSQLField() bool {
	return this.SQLPrimary() || this.SQLNotNull() || this.SQLNoUpdate()
}

func (this *TprotoField) GoName() string {
	return proto.GoCamelCase(this.Name)
}

func (this *TprotoMsg) GoName() string {
	return proto.GoCamelCase(this.Name)
}
