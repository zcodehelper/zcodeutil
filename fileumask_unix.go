//go:build !windows

package zcodeutil

import "syscall"

// SysUmask fix mkdirall 0777 -> 0775 in linux, need SysUmask(0)
func SysUmask(mask int) (oldmask int) {
	return syscall.Umask(mask)
}
