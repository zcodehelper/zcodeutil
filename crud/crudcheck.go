package crud

import (
	"errors"
	"gitlab.com/zcodehelper/zcodeutil"
	"golang.org/x/exp/slices"
	"strconv"
	"strings"
)

func IsColumnNamesOK(columnNames []string) error {
	for i := range columnNames {
		col := columnNames[i]
		if !zcodeutil.IsValidID(col) {
			return errors.New("column name is invalid:" + col)
		}
	}
	return nil
}

func IsColumnSecurity(columnExpression string) error {
	if strings.ContainsAny(columnExpression, "/*#,;`") {
		return errors.New("bad column expression(/*#,;`):" + columnExpression)
	}
	if strings.Contains(columnExpression, "--") {
		return errors.New("bad column expression(--):" + columnExpression)
	}
	return nil
}
func IsOperatorSecurity(operator string) error {
	return IsColumnSecurity(operator)
}

func IsSelectParamOK(sp *SelectParam) error {
	if sp == nil {
		return nil
	}
	err := IsColumnNamesOK(sp.KeyColumns)
	if err != nil {
		return err
	}

	return IsColumnNamesOK(sp.ResultColumns)
}

func QueryParamValidate(qp *QueryParam) error {
	resultColumnsOK := IsColumnNamesOK(qp.ResultColumn)
	if resultColumnsOK != nil {
		return resultColumnsOK
	}
	for i := range qp.TimeColumn {
		timeCol := qp.TimeColumn[i]
		if i%2 == 0 {
			//column name
			if !zcodeutil.IsValidID(timeCol) {
				return errors.New("time column name is invalid:" + timeCol)
			}
			inResultColumn := slices.Contains(qp.ResultColumn, timeCol)
			if !inResultColumn {
				return errors.New("time column name is not in result column:" + timeCol)
			}
		} else {
			//time zone minutes
			_, atoiErr := strconv.Atoi(timeCol)
			if atoiErr != nil {
				return errors.New("time zone value is invalid:" + timeCol + " for " + qp.TimeColumn[i-1])
			}
		}
	}
	for i := range qp.Where {
		whereItem := qp.Where[i]
		isColumnSecurity := IsColumnSecurity(whereItem.Field)
		if isColumnSecurity != nil {
			return isColumnSecurity
		}
		isOperatorSecurity := IsOperatorSecurity(whereItem.FieldCompareOperator)
		if isOperatorSecurity != nil {
			return isOperatorSecurity
		}
	}

	return nil

}
