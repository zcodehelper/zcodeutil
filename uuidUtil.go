package zcodeutil

import (
	"crypto/md5"

	"github.com/google/uuid"
	uuidv7 "github.com/uuid6/uuid6go-proto"
)

var UUIDzero = uuid.UUID{}

func NewUUIDv1() uuid.UUID {
	return uuid.Must(uuid.NewUUID())
}
func NewUUIDv4() uuid.UUID {
	return uuid.New()
}

var uuidv7gen uuidv7.UUIDv7Generator

func NewUUIDv7() uuid.UUID {
	id := uuidv7gen.Next()
	return uuid.UUID(id)
}

// calculate a uuid from str using md5
func StrHash2UUID(str string) (uuid.UUID, error) {
	hash := md5.Sum(StrToBytesUnsafe(str))

	return uuid.FromBytes(hash[:16])

}

// return the underlying byte array of a uuid [16]byte
func UUID2Bytes(uuid uuid.UUID) []byte {
	uuiddArray := ([16]byte)(uuid)
	return uuiddArray[:]

}

// [16]byte -> uuid, on err return uuidzero
func Bytes2UUID(uuidbytes []byte) uuid.UUID {
	uid, err := uuid.FromBytes(uuidbytes)
	if err != nil {
		return UUIDzero
	}

	return uid
}
