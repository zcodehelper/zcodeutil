module gitlab.com/zcodehelper/zcodeutil

go 1.18

require (
	github.com/google/uuid v1.3.0
	github.com/uuid6/uuid6go-proto v0.2.1
	golang.org/x/exp v0.0.0-20221211140036-ad323defaf05
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
)
