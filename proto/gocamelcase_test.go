package proto

import "testing"

func TestGoCamelCase(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test_e2ee_type",
			args: args{
				s: "e2ee_type",
			},
			want: "E2EeType",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GoCamelCase(tt.args.s); got != tt.want {
				t.Errorf("GoCamelCase() = %v, want %v", got, tt.want)
			}
		})
	}
}
