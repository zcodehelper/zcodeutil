//go:build windows

package zcodeutil

// SysUmask fix mkdirall 0777 -> 0775 in linux, need SysUmask(0)
func SysUmask(mask int) (oldmask int) {
	return 0666
}
