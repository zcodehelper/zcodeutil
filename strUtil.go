package zcodeutil

//IsASCIILower is ascii lower
func IsASCIILower(c byte) bool {
	return 'a' <= c && c <= 'z'
}

//IsASCIIUpper is ascii upper
func IsASCIIUpper(c byte) bool {
	return 'A' <= c && c <= 'Z'
}

//IsASCIIDigit is ascii digit
func IsASCIIDigit(c byte) bool {
	return '0' <= c && c <= '9'
}

//IsValidID is valid id name
func IsValidID(id string) bool {
	if len(id) == 0 {
		return false
	}
	firstChar := id[0]
	if IsASCIIDigit(firstChar) {
		return false
	}
	for i := 1; i < len(id); i++ {
		c := id[i]
		if !(IsASCIILower(c) || IsASCIIUpper(c) || IsASCIIDigit(c) || c == '_') {
			return false
		}
	}
	return true
}
