package crud

import (
	"context"
	"encoding/base64"
	"errors"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/proto"
)

var EnometaFound = errors.New("no metadata in context")

func SelectParamInMetadata(ctx context.Context) (selectParam *SelectParam, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, EnometaFound
	}

	t := md.Get("select-param")
	if len(t) == 0 {
		return nil, EnometaFound
	}
	selectParamB64 := t[0]
	selectParamPbBin, err := base64.StdEncoding.DecodeString(selectParamB64)
	if err != nil {
		return nil, err
	}
	selectParam = &SelectParam{}
	err = proto.Unmarshal(selectParamPbBin, selectParam)
	if err != nil {
		return nil, err
	} else {
		return selectParam, nil
	}
}
