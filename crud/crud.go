package crud

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/zcodehelper/zcodeutil"
	"gitlab.com/zcodehelper/zcodeutil/zreflect"
	"golang.org/x/exp/slices"
)

// TSQLDIALECT sql dialect,only support sqlite,mysql,postgresql now
type TSQLDIALECT int32

const TSQLDIALECT_UNKNOWN TSQLDIALECT = 0
const TSQLDIALECT_SQLITE TSQLDIALECT = 1
const TSQLDIALECT_MYSQL TSQLDIALECT = 2
const TSQLDIALECT_POSTGRESQL TSQLDIALECT = 3

type TSQLPlaceolder string

const TSQLPlaceolder_QUESTION TSQLPlaceolder = "?"
const TSQLPlaceolder_DOLLAR TSQLPlaceolder = "$"

type ITprotoMsg interface {
	TprotoMsg(cache bool) *TprotoMsg
}

// SqlDialectPlaceHolder return placeholder for sql dialect
func SqlDialectPlaceHolder(sqlDialect TSQLDIALECT) TSQLPlaceolder {
	switch sqlDialect {
	case TSQLDIALECT_SQLITE:
		return TSQLPlaceolder_QUESTION
	case TSQLDIALECT_MYSQL:
		return TSQLPlaceolder_QUESTION
	case TSQLDIALECT_POSTGRESQL:
		return TSQLPlaceolder_DOLLAR
	default:
		return TSQLPlaceolder_QUESTION
	}
}

// no=0: first key value
func (this *SelectParam) getKeyValue(no int) string {
	if no >= len(this.KeyValues) || this == nil {
		return ""
	}
	return this.KeyValues[no]
}

func Insert(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder) (r *DMLResult, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildInsert(protomsg, placeholder)
	if err != nil {
		return nil, err
	}
	sqlResult, err := db.Exec(sqlStr, sqlVals...)
	if err != nil {
		return nil, err
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return nil, err
	}
	r = &DMLResult{
		AffectedRow: int32(rowsAffected),
	}
	return r, nil
}
func Update(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder) (r *DMLResult, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildUpdate(protomsg, placeholder)
	if err != nil {
		return nil, err
	}
	sqlResult, err := db.Exec(sqlStr, sqlVals...)
	if err != nil {
		return nil, err
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return nil, err
	}
	r = &DMLResult{
		AffectedRow: int32(rowsAffected),
	}
	return r, nil
}

// PartialUpdate update partialColumns value by keys columns
// if keys is empty, use primary key
func PartialUpdate(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder, partialColumns []string, keys []string) (r *DMLResult, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildPartialUpdate(protomsg, placeholder, partialColumns, keys)
	if err != nil {
		return nil, err
	}
	sqlResult, err := db.Exec(sqlStr, sqlVals...)
	if err != nil {
		return nil, err
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return nil, err
	}
	r = &DMLResult{
		AffectedRow: int32(rowsAffected),
	}
	return r, nil
}
func Delete(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder) (r *DMLResult, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildDelete(protomsg, placeholder)
	if err != nil {
		return nil, err
	}
	sqlResult, err := db.Exec(sqlStr, sqlVals...)
	if err != nil {
		return nil, err
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return nil, err
	}
	r = &DMLResult{
		AffectedRow: int32(rowsAffected),
	}
	return r, nil
}
func SelectOne(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder, selectParam *SelectParam) (selectRow int, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildSelectOne(protomsg, placeholder, selectParam)
	if err != nil {
		return 0, err
	}

	rows, err := db.Query(sqlStr, sqlVals...)
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		return 0, err
	}
	columnNames, err := rows.Columns()
	if err != nil {
		return 0, err
	}
	if rows.Next() {
		rowVals := make([]interface{}, len(columnNames))
		for i := range rowVals {
			rowVals[i] = new(interface{})
		}
		err := rows.Scan(rowVals...)
		if err != nil {
			fmt.Println("scan err:", err)
			return 0, err
		}

		err = tprotomsg.StructScan(protomsg, columnNames, rowVals, nil)
		if err != nil {
			return 0, err
		}
		return 1, nil

	} else {
		return 0, nil
	}

}

// SelectMany select many rows from table using selectParam as condition
// the return value rows must be closed manually
func SelectMany(db *sql.DB, protomsg ITprotoMsg, placeholder TSQLPlaceolder, selectParam *SelectParam) (rows *sql.Rows, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildSelectMany(placeholder, selectParam)
	if err != nil {
		return nil, err
	}
	rows, err = db.Query(sqlStr, sqlVals...)
	if err != nil {
		if rows != nil {
			rows.Close()
		}
		return nil, err
	}
	return rows, nil
}

// Query query table according to queryParam
// the return value rows must be closed manually
func Query(db *sql.DB, protomsg ITprotoMsg, sqlSchema TSQLDIALECT, queryParam *QueryParam, where_1e1 bool) (rows *sql.Rows, err error) {
	tprotomsg := protomsg.TprotoMsg(false)
	sqlStr, sqlVals, err := tprotomsg.BuildQuery(sqlSchema, queryParam, where_1e1)
	if err != nil {
		return nil, err
	}

	rows, err = db.Query(sqlStr, sqlVals...)
	if err != nil {
		if rows != nil {
			rows.Close()
		}

		return nil, err
	}
	return rows, err
}
func (this *TprotoMsg) BuildInsert(msgobj interface{}, placeholder TSQLPlaceolder) (sqlStr string, vals []interface{}, err error) {

	sb := &strings.Builder{}
	sb.WriteString(SQL_INSERT_INTO)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_LEFT_PARENTHESES)
	firstCoumn := true
	columntCount := 0
	for _, field := range this.Fields {
		if field.SQLNot() {
			continue
		}
		val, err := zreflect.GetField(msgobj, field.GoName())
		if err != nil {
			err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
			return "", nil, err
		}

		if firstCoumn {
			firstCoumn = false
		} else {
			sb.WriteString(SQL_COMMA)
		}
		columntCount++
		sb.WriteString(field.Name)
		if !field.SQLNotNull() && (field.ForeighRef() || field.EmptyAsNull()) {
			val = maybeNull(val, field)
		}
		vals = append(vals, val)
	}
	sb.WriteString(SQL_RIGHT_PARENTHESES)
	sb.WriteString(SQL_INSERT_VALUES)
	sb.WriteString(SQL_LEFT_PARENTHESES)

	firstPlaceholder := true

	for i := 0; i < columntCount; i++ {
		if firstPlaceholder {
			firstPlaceholder = false
		} else {
			sb.WriteString(SQL_COMMA)
		}
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(i + 1))
		}
	}

	sb.WriteString(SQL_RIGHT_PARENTHESES)
	sb.WriteString(SQL_SEMICOLON)

	return sb.String(), vals, nil
}

func (this *TprotoMsg) BuildUpdate(msgobj interface{}, placeholder TSQLPlaceolder) (sqlStr string, vals []interface{}, err error) {
	sb := strings.Builder{}

	sb.WriteString(SQL_UPDATE)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_SET)

	primaryFields := make([]TprotoField, 0)
	updateFields := make([]TprotoField, 0)
	for _, field := range this.Fields {
		if field.SQLNot() || field.SQLNoUpdate() {
			continue
		}

		if field.SQLPrimary() {
			primaryFields = append(primaryFields, field)
			continue
		}

		updateFields = append(updateFields, field)
	}

	for i, field := range updateFields {
		val, err := zreflect.GetField(msgobj, field.GoName())
		if err != nil {
			err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
			return "", nil, err
		}
		if i != 0 {
			sb.WriteString(SQL_COMMA)
		}
		sb.WriteString(field.Name)
		sb.WriteString(SQL_EQUEAL)
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(i + 1))
		}
		if !field.SQLNotNull() && (field.ForeighRef() || field.EmptyAsNull()) {
			val = maybeNull(val, field)
		}
		vals = append(vals, val)
	}

	sb.WriteString(SQL_WHERE)
	for i, field := range primaryFields {
		val, err := zreflect.GetField(msgobj, field.GoName())
		if err != nil {
			err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
			return "", nil, err
		}

		if i != 0 {
			sb.WriteString(SQL_AND)
		}
		sb.WriteString(field.Name)
		sb.WriteString(SQL_EQUEAL)
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(i + 1 + len(updateFields)))
		}
		vals = append(vals, val)
	}

	sb.WriteString(SQL_SEMICOLON)

	return sb.String(), vals, nil
}

// BuildPartialUpdate update partialColumns value by keys columns
// if keys is empty, use primary key
func (this *TprotoMsg) BuildPartialUpdate(msgobj interface{}, placeholder TSQLPlaceolder, partialColumns []string, keys []string) (sqlStr string, vals []interface{}, err error) {
	sb := strings.Builder{}

	sb.WriteString(SQL_UPDATE)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_SET)

	fieldMap := make(map[string]TprotoField, 0)
	primaryFields := make([]TprotoField, 0)
	updateFields := make([]TprotoField, 0)
	for _, field := range this.Fields {
		if field.SQLNot() || field.SQLNoUpdate() {
			continue
		}

		fieldMap[field.Name] = field

		if field.SQLPrimary() {
			primaryFields = append(primaryFields, field)
		}
	}

	for _, column := range partialColumns {
		field, ok := fieldMap[column]
		if !ok {
			err = fmt.Errorf("column %s not found in %s", column, this.Name)
			return "", nil, err
		}
		updateFields = append(updateFields, field)
	}

	if len(keys) > 0 {
		keysFields := make([]TprotoField, 0)
		for _, key := range keys {
			field, ok := fieldMap[key]
			if !ok {
				err = fmt.Errorf("column %s not found in %s", key, this.Name)
				return "", nil, err
			}
			keysFields = append(keysFields, field)
		}

		primaryFields = keysFields
	}

	for i, field := range updateFields {
		val, err := zreflect.GetField(msgobj, field.GoName())
		if err != nil {
			err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
			return "", nil, err
		}
		if i != 0 {
			sb.WriteString(SQL_COMMA)
		}
		sb.WriteString(field.Name)
		sb.WriteString(SQL_EQUEAL)
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(i + 1))
		}
		if !field.SQLNotNull() && (field.ForeighRef() || field.EmptyAsNull()) {
			val = maybeNull(val, field)
		}
		vals = append(vals, val)
	}

	sb.WriteString(SQL_WHERE)
	for i, field := range primaryFields {
		val, err := zreflect.GetField(msgobj, field.GoName())
		if err != nil {
			err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
			return "", nil, err
		}

		if i != 0 {
			sb.WriteString(SQL_AND)
		}
		sb.WriteString(field.Name)
		sb.WriteString(SQL_EQUEAL)
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(i + 1 + len(updateFields)))
		}
		vals = append(vals, val)
	}

	sb.WriteString(SQL_SEMICOLON)

	return sb.String(), vals, nil

}

// empty string and integer 0 is null
func maybeNull(val interface{}, field TprotoField) interface{} {
	valStr := fmt.Sprint(val)
	if len(valStr) == 0 || valStr == "0" {
		return sql.NullString{String: "", Valid: false}
	}
	return val
}

func (this *TprotoMsg) BuildDelete(msgobj interface{}, placeholder TSQLPlaceolder) (sqlStr string, vals []interface{}, err error) {
	sb := strings.Builder{}

	sb.WriteString(SQL_DELETE)
	sb.WriteString(SQL_FROM)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_WHERE)
	posNo := 1
	for _, field := range this.Fields {
		if field.SQLPrimary() {
			val, err := zreflect.GetField(msgobj, field.GoName())
			if err != nil {
				err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
				return "", nil, err
			}
			if posNo != 1 {
				sb.WriteString(SQL_AND)
			}
			sb.WriteString(field.Name)
			sb.WriteString(SQL_EQUEAL)

			if placeholder == TSQLPlaceolder_QUESTION {
				sb.WriteString(string(TSQLPlaceolder_QUESTION))
			} else {
				sb.WriteString(string(TSQLPlaceolder_DOLLAR))
				sb.WriteString(strconv.Itoa(posNo))
				posNo++
			}
			vals = append(vals, val)
		}
	}

	sb.WriteString(SQL_SEMICOLON)

	return sb.String(), vals, nil

}

// BuildSelect select one row by primary key
// if selectParam=nil use key value from msgobj
func (this *TprotoMsg) BuildSelectOne(msgobj interface{}, placeholder TSQLPlaceolder, selectParam *SelectParam) (sqlStr string, vals []interface{}, err error) {
	err = IsSelectParamOK(selectParam)
	if err != nil {
		return "", nil, err
	}
	sb := strings.Builder{}

	selectParamIsNil := selectParam == nil
	if selectParamIsNil {
		selectParam = &SelectParam{}
	}

	selectParamNoKeyValues := len(selectParam.KeyValues) == 0

	if len(selectParam.ResultColumns) == 0 {
		selectParam.ResultColumns = []string{" * "}
	}

	sb.WriteString(SQL_SELECT)
	for i, column := range selectParam.ResultColumns {
		if i != 0 {
			sb.WriteString(SQL_COMMA)
		}
		sb.WriteString(column)
	}

	sb.WriteString(SQL_FROM)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_WHERE)

	if len(selectParam.KeyColumns) == 0 {
		//use primary key
		posNo := 1
		for _, field := range this.Fields {
			if field.SQLPrimary() {
				var val interface{}
				if selectParamIsNil || selectParamNoKeyValues {
					val, err = zreflect.GetField(msgobj, field.GoName())
					if err != nil {
						err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
						return "", nil, err
					}
				} else {
					val = selectParam.getKeyValue(posNo - 1)
				}

				if posNo != 1 {
					sb.WriteString(SQL_AND)
				}
				sb.WriteString(field.Name)
				sb.WriteString(SQL_EQUEAL)

				vals = append(vals, val)

				if placeholder == TSQLPlaceolder_QUESTION {
					sb.WriteString(string(TSQLPlaceolder_QUESTION))
				} else {
					sb.WriteString(string(TSQLPlaceolder_DOLLAR))
					sb.WriteString(strconv.Itoa(posNo))
				}
				posNo++
			}
		}
	} else {
		//use key columns in selectParam
		posNo := 1
		for _, field := range this.Fields {
			if (field.SQLPrimary() || field.SQLUnique()) && (slices.Contains(selectParam.KeyColumns, field.Name) || slices.Contains(selectParam.KeyColumns, strings.ToLower(field.Name))) {
				var val interface{}
				if selectParamIsNil || selectParamNoKeyValues {
					val, err = zreflect.GetField(msgobj, field.GoName())
					if err != nil {
						err = fmt.Errorf("get field err: %s.%s %w", this.Name, field.Name, err)
						return "", nil, err
					}
				} else {
					val = selectParam.getKeyValue(posNo - 1)
				}
				if posNo != 1 {
					sb.WriteString(SQL_AND)
				}
				sb.WriteString(field.Name)
				sb.WriteString(SQL_EQUEAL)

				vals = append(vals, val)

				if placeholder == TSQLPlaceolder_QUESTION {
					sb.WriteString(string(TSQLPlaceolder_QUESTION))
				} else {
					sb.WriteString(string(TSQLPlaceolder_DOLLAR))
					sb.WriteString(strconv.Itoa(posNo))
				}
				posNo++
			}
		}
	}
	sb.WriteString(SQL_LIMIT_1)

	sb.WriteString(SQL_SEMICOLON)

	return sb.String(), vals, nil

}

// BuildSelect select multiple rows by primary key or unique columns
func (this *TprotoMsg) BuildSelectMany(placeholder TSQLPlaceolder, selectParam *SelectParam) (sqlStr string, vals []interface{}, err error) {
	err = IsSelectParamOK(selectParam)
	if err != nil {
		return "", nil, err
	}
	if len(selectParam.KeyValues) == 0 {
		return "", nil, errors.New("keyValues is empty")
	}

	sb := strings.Builder{}

	if len(selectParam.ResultColumns) == 0 {
		selectParam.ResultColumns = []string{" * "}
	}

	sb.WriteString(SQL_SELECT)
	for i, column := range selectParam.ResultColumns {
		if i != 0 {
			sb.WriteString(SQL_COMMA)
		}
		sb.WriteString(column)
	}

	sb.WriteString(SQL_FROM)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_WHERE)
	var primariyFieldList []TprotoField
	if len(selectParam.KeyColumns) > 0 {
		//use key columns in selectParam
		fieldmap := this.BuildFieldMap()
		for i := range selectParam.KeyColumns {
			keyColumn := selectParam.KeyColumns[i]
			field, ok := fieldmap[keyColumn]
			if !ok {
				return "", nil, errors.New("keyColumn not found:" + keyColumn)
			}
			if !(field.SQLPrimary() || field.SQLUnique()) {
				return "", nil, errors.New("keyColumn not primary or unique:" + keyColumn)
			}
			primariyFieldList = append(primariyFieldList, field)
		}

	} else {
		//use primary key
		primariyFieldList = this.BuildPrimaryFieldList()
	}

	if len(primariyFieldList) == 1 {
		sb.WriteString(primariyFieldList[0].Name)
		sb.WriteString(SQL_EQUEAL)
		sb.WriteString(SQL_ANY)
		sb.WriteString(SQL_LEFT_PARENTHESES)
		if placeholder == TSQLPlaceolder_QUESTION {
			sb.WriteString(string(TSQLPlaceolder_QUESTION))
		} else {
			sb.WriteString(string(TSQLPlaceolder_DOLLAR))
			sb.WriteString(strconv.Itoa(1))
		}
		sb.WriteString(SQL_RIGHT_PARENTHESES)
		vals = append(vals, selectParam.KeyValues)
	} else {
		rowCount := len(selectParam.KeyValues) / len(primariyFieldList)
		posNo := 1
		for i := 0; i < rowCount; i++ {
			if i != 0 {
				sb.WriteString(SQL_OR)
			}
			sb.WriteString(SQL_LEFT_PARENTHESES)
			for j := 0; j < len(primariyFieldList); j++ {
				if j != 0 {
					sb.WriteString(SQL_AND)
				}
				sb.WriteString(primariyFieldList[j].Name)
				sb.WriteString(SQL_EQUEAL)
				if placeholder == TSQLPlaceolder_QUESTION {
					sb.WriteString(string(TSQLPlaceolder_QUESTION))
				} else {
					sb.WriteString(string(TSQLPlaceolder_DOLLAR))
					sb.WriteString(strconv.Itoa(posNo))
					posNo++
				}
				vals = append(vals, selectParam.KeyValues[i*len(primariyFieldList)+j])
			}
			sb.WriteString(SQL_RIGHT_PARENTHESES)
		}

	}

	sb.WriteString(SQL_SEMICOLON)
	return sb.String(), vals, nil
}

// BuildQuery query table using queryParam
// where_1e1: add 1=1 to where clause, if true then queryParam can have empty where condition
func (this *TprotoMsg) BuildQuery(sqlSchema TSQLDIALECT, queryParam *QueryParam, where_1e1 bool) (sqlStr string, vals []interface{}, err error) {
	err = QueryParamValidate(queryParam)
	if err != nil {
		return "", nil, err
	}
	placeholder := TSQLPlaceolder_QUESTION
	if sqlSchema == TSQLDIALECT_POSTGRESQL {
		placeholder = TSQLPlaceolder_DOLLAR
	}

	timezoneOfColumns := make(map[string]int)
	if len(queryParam.TimeColumn) > 0 {
		for i := 1; i <= len(queryParam.TimeColumn); i += 2 {
			col := queryParam.TimeColumn[i-1]
			timezoneMinutes, err := strconv.Atoi(queryParam.TimeColumn[i])
			if err != nil {
				return "", nil, err
			}
			timezoneOfColumns[col] = timezoneMinutes
		}
	}

	sb := strings.Builder{}
	sb.WriteString(SQL_SELECT)
	if len(queryParam.ResultColumn) == 0 {
		if len(queryParam.TimeColumn) == 0 {
			queryParam.ResultColumn = []string{" * "}
		} else {
			for i := range this.Fields {
				field := this.Fields[i]
				if field.SQLNot() {
					continue
				}
				queryParam.ResultColumn = append(queryParam.ResultColumn, field.Name)
			}
		}
	}
	for i, column := range queryParam.ResultColumn {
		if i != 0 {
			sb.WriteString(SQL_COMMA)
		}
		timezoneMinutes, ok := timezoneOfColumns[column]
		if ok && timezoneMinutes != 0 {
			switch sqlSchema {
			case TSQLDIALECT_POSTGRESQL:
				sb.WriteString(column)
				sb.WriteString(SQL_SPACE)
				if timezoneMinutes > 0 {
					sb.WriteString(SQL_PLUS)
				} else {
					sb.WriteString(SQL_MINUS)
				}
				sb.WriteString(SQL_INTERVAL)
				sb.WriteString(SQL_APOSTROPHE)
				if timezoneMinutes > 0 {
					sb.WriteString(strconv.Itoa(timezoneMinutes))
				} else {
					sb.WriteString(strconv.Itoa(-timezoneMinutes))
				}
				sb.WriteString(SQL_MINUTES)
				sb.WriteString(SQL_APOSTROPHE)
				sb.WriteString(SQL_AS)
				sb.WriteString(column)

			case TSQLDIALECT_MYSQL:
				sb.WriteString(SQL_TIMESTAMPADD)
				sb.WriteString(SQL_LEFT_PARENTHESES)
				sb.WriteString(SQL_MINUTE)
				sb.WriteString(SQL_COMMA)
				sb.WriteString(strconv.Itoa(timezoneMinutes))
				sb.WriteString(SQL_COMMA)
				sb.WriteString(column)
				sb.WriteString(SQL_RIGHT_PARENTHESES)
				sb.WriteString(SQL_AS)
				sb.WriteString(column)

			case TSQLDIALECT_SQLITE:
				sb.WriteString(sql_DATETIME)
				sb.WriteString(SQL_LEFT_PARENTHESES)
				sb.WriteString(column)
				sb.WriteString(SQL_COMMA)
				sb.WriteString(SQL_APOSTROPHE)
				if timezoneMinutes > 0 {
					sb.WriteString(SQL_PLUS)
				}
				sb.WriteString(strconv.Itoa(timezoneMinutes))
				sb.WriteString(SQL_MINUTES)
				sb.WriteString(SQL_APOSTROPHE)
				sb.WriteString(SQL_RIGHT_PARENTHESES)
				sb.WriteString(SQL_AS)
				sb.WriteString(column)
			case TSQLDIALECT_UNKNOWN:
				fallthrough
			default:
				return "", nil, errors.New("unsupported sql dialect")

			}
		} else {
			sb.WriteString(column)
		}
	}
	sb.WriteString(SQL_FROM)
	sb.WriteString(this.Name)
	sb.WriteString(SQL_WHERE)
	if where_1e1 {
		sb.WriteString(SQL_1E1)
	} else {
		if len(queryParam.Where) == 0 {
			return "", nil, errors.New("where is empty")
		}
	}

	isFirstCondition := !where_1e1
	posNo := 1
	for _, condition := range queryParam.Where {
		if isFirstCondition {
			isFirstCondition = false
		} else {
			sb.WriteString(SQL_AND)
		}
		sb.WriteString(SQL_LEFT_PARENTHESES)
		sb.WriteString(condition.Field)
		sb.WriteString(SQL_SPACE)
		sb.WriteString(condition.FieldCompareOperator)
		sb.WriteString(SQL_SPACE)
		if len(condition.FieldValues) > 0 {
			sb.WriteString(SQL_LEFT_PARENTHESES)
			for i := range condition.FieldValues {
				if i != 0 {
					sb.WriteString(SQL_COMMA)
				}
				if placeholder == TSQLPlaceolder_QUESTION {
					sb.WriteString(string(TSQLPlaceolder_QUESTION))
				} else {
					sb.WriteString(string(TSQLPlaceolder_DOLLAR))
					sb.WriteString(strconv.Itoa(posNo))
					posNo++
				}
				sb.WriteString(SQL_RIGHT_PARENTHESES)
				vals = append(vals, condition.FieldValues[i])
			}
			sb.WriteString(SQL_RIGHT_PARENTHESES)
		} else {
			if placeholder == TSQLPlaceolder_QUESTION {
				sb.WriteString(string(TSQLPlaceolder_QUESTION))
			} else {
				sb.WriteString(string(TSQLPlaceolder_DOLLAR))
				sb.WriteString(strconv.Itoa(posNo))
				posNo++
			}
			vals = append(vals, condition.FieldValue)
		}
		sb.WriteString(SQL_RIGHT_PARENTHESES)

	}

	if len(queryParam.OrderBy) > 0 {
		sb.WriteString(SQL_ORDER_BY)
		for i, v := range queryParam.OrderBy {
			colAscDesc := strings.Split(v, " ")
			if len(colAscDesc) != 2 {
				return "", nil, errors.New("order by column format error:" + v)
			}

			ok := zcodeutil.IsValidID(colAscDesc[0])
			if !ok {
				return "", nil, errors.New("order by column name format error:" + v)
			}

			ascDesc := strings.ToLower(colAscDesc[1])
			if ascDesc != "asc" && ascDesc != "desc" {
				return "", nil, errors.New("order by column asc/desc format error:" + v)
			}

			sb.WriteString(colAscDesc[0])
			sb.WriteString(SQL_SPACE)
			sb.WriteString(ascDesc)
			if i != len(queryParam.OrderBy)-1 {
				sb.WriteString(SQL_COMMA)
			}
		}
		if queryParam.Limit != 0 {
			sb.WriteString(SQL_LIMIT)
			sb.WriteString(strconv.FormatUint(uint64(queryParam.Limit), 10))
		}
		sb.WriteString(SQL_OFFSET)
		sb.WriteString(strconv.FormatUint(uint64(queryParam.Offset), 10))
	}

	sb.WriteString(SQL_SEMICOLON)
	return sb.String(), vals, nil

}
func (this *TprotoMsg) BuildPrimaryFieldList() (fl []TprotoField) {
	for _, field := range this.Fields {
		if field.SQLPrimary() {
			fl = append(fl, field)
		}
	}
	return fl
}
func (this *TprotoMsg) BuildPrimaryFieldMap() map[string]TprotoField {
	primaryFieldMap := make(map[string]TprotoField)
	for _, field := range this.Fields {
		if field.SQLPrimary() {
			primaryFieldMap[field.Name] = field
		}
	}
	return primaryFieldMap
}

func (this *TprotoMsg) BuildFieldMap() map[string]TprotoField {
	fieldMap := make(map[string]TprotoField, len(this.Fields)*3)
	for i := range this.Fields {
		field := this.Fields[i]
		fieldMap[field.Name] = field
		fieldMap[field.GoName()] = field
		fieldMap[strings.ToLower(field.Name)] = field
	}
	return fieldMap
}

func (this *TprotoMsg) StructScan(msgobj interface{}, sqlColumnNames []string, vals []any, fieldMap map[string]TprotoField) (err error) {
	if len(sqlColumnNames) != len(vals) {
		return errors.New("sqlColumnNames and vals length not equal")
	}
	if fieldMap == nil {
		fieldMap = this.BuildFieldMap()
	}

	for i := range sqlColumnNames {
		field, ok := fieldMap[sqlColumnNames[i]]
		if !ok {
			field, ok = fieldMap[strings.ToLower(sqlColumnNames[i])]
		}
		if !ok {
			return fmt.Errorf("field not found: %s in %s", sqlColumnNames[i], this.Name)
		}

		err = zreflect.SetField(msgobj, field.GoName(), *(vals[i].(*interface{})))
		if err != nil {
			return fmt.Errorf("set field err: %s.%s %w", this.Name, field.Name, err)
		}
	}

	return nil
}
